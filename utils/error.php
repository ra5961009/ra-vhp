<?php
function error($e) {
  print_r($e, "----");
  $error = explode(";", $e->getMessage());
  $code = 500;
  $message = 'Internal Server Error.';

  if (sizeof($error) > 1) {
    $code = $error[0];
    $message = $error[1];
  } else {
    print_r($error[0]);
    if (isset($error[0])) $message = $error[0];
  }

  http_response_code($code);
  echo json_encode(array("message" => $message));
  exit(); 
}