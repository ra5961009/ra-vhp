<?php
include_once("controllers/memberController.php");
include_once("controllers/transactionController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\MemberController;
use Controller\transactionController;
$memberController = new MemberController();
$transactionController = new TransactionController();
$path = isset($pathParts[3]) ? $pathParts[3] : '';

external();
switch ($_SERVER['REQUEST_METHOD']) {
  case "POST":
    try {
      if(!isset($pathParts[4])) {
        switch($path) {
          case 'hotel':
            $transactionController->hotel($body);
          break;
          case 'outlet':
            $transactionController->outlet($body);
          break;
          default:
            http_response_code(404);
            echo json_encode(array("message" => "Page not found"));        
        }
      } else {
        http_response_code(404);
        echo json_encode(array("message" => "Page not found"));        
      }
    } catch (\Exception $e) {
      error($e);
    }
    break;
  default:
    http_response_code(405);
    echo json_encode(array("message" => "Method not allowed"));
}