<?php
include_once("controllers/memberController.php");
include_once("controllers/voucherController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\MemberController;
use Controller\VoucherController;
$memberController = new MemberController();
$voucherController = new VoucherController();
$path = isset($pathParts[3]) ? $pathParts[3] : '';

external();
switch ($_SERVER['REQUEST_METHOD']) {
  case "POST":
    try {
      if(!isset($pathParts[4])) {
        switch($path) {
          case 'burn':
            $voucherController->burn($body);
          break;
          case 'unburn':
            $voucherController->unburn($body);
          break;
          default:
            http_response_code(404);
            echo json_encode(array("message" => "Page not found"));        
        }
      } else {
        http_response_code(404);
        echo json_encode(array("message" => "Page not found"));        
      }
    } catch (\Exception $e) {
      error($e);
    }
    break;
    case "GET":
      if ($path) {
          $result = $voucherController->getByHotelsId($path);
          http_response_code(200);
          echo json_encode(array("voucher" => $result));
      } else {
          $results = $voucherController->getAll($query);
          http_response_code(200);
          echo json_encode($results);
      }
      break;
  default:
    http_response_code(405);
    echo json_encode(array("message" => "Method not allowed"));
}