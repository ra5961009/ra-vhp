<?php
include_once("utils/error.php");
include_once("controllers/tokenController.php");

use Controller\TokenController;
$tokenController = new TokenController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
  case "GET":
    if (isset($query->key)) {
      if ($query->key == '1q2w3e4r5t6y') {
        $results = $tokenController->generateToken();
        http_response_code(200);
        echo json_encode(array("token" => $results));
      } else {
        http_response_code(401);
        echo json_encode(array("message" => "Invalid key!"));
      }
    } else {
      http_response_code(405);
      echo json_encode(array("message" => "Key is required!"));
    }
  break;
  default:
    http_response_code(405);
    echo json_encode(array("message" => "Method not allowed"));
}