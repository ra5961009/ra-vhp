<?php
include_once("controllers/memberController.php");
include_once("controllers/voucherController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\MemberController;
$memberController = new MemberController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

external();
switch ($_SERVER['REQUEST_METHOD']) {
  case "GET":
    if ($param == "benefitOutlet") {
      $result = $memberController->getVouchers($query);
      if ($result) {
        http_response_code(200);
        echo json_encode(array(
            "data" => $result,
            "message" => "Success",
            "statusCode" => "SUCCESS_GET_REQUEST",
            "status" => true
        ));
      } else {
        http_response_code(200);
        echo json_encode(array(
            "message" => "The data was not found.",
            "statusCode" => "INFO_DATA_NOT_FOUND",
            "status" => true
        ));
      }
    } else {
      if ($param) {
        $result = $memberController->getByPhone($param);
        if ($result) {
          http_response_code(200);
          echo json_encode(array(
              "data" => $result,
              "message" => "Success",
              "statusCode" => "SUCCESS_GET_REQUEST",
              "status" => true
          ));
        } else {
          http_response_code(200);
          echo json_encode(array(
              "message" => "The data was not found.",
              "statusCode" => "INFO_DATA_NOT_FOUND",
              "status" => true
          ));
        }
      } else {
        http_response_code(400);
        echo json_encode(array("message" => "Member ID is required!"));
      }
    }
    break;
  case "POST":
    try {
      $results = $memberController->synchronize($body);
    } catch (\Exception $e) {
      error($e);
    }
    break;
  default:
    http_response_code(405);
    echo json_encode(array("message" => "Method not allowed"));
}