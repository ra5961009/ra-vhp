<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header('Content-Type: application/json; charset=utf-8; Authorization');
header_remove("X-Powered-By");
header_remove('Server');

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

define('JWT_SECRET', '1qs2wd3ef4rg5th6yj7uk8il9o;');
// define('PASSWORD_DEFAULT', 'ra-1234');

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);
}

$content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
$uri = $_SERVER['REQUEST_URI'];
$url = explode("?", $uri);

$path = trim($url[0], '/');
$path = filter_var($path, FILTER_SANITIZE_URL);
$pathParts = explode('/', $path);
$query_params = isset($url[1]) ? explode("&", $url[1]) : [];
$query = [];
if(sizeof($query_params) > 0) {
    foreach($query_params as $q) {
        $o = explode("=", $q);
        $query[$o[0]] = $o[1];
    }
}
$request = isset($pathParts[2]) ? $pathParts[2] : null;
$query = (object) $query;
$body = (object)[];
if ($content_type == 'application/json') {
    $body = json_decode(file_get_contents("php://input"));
} elseif ($content_type == 'application/x-www-form-urlencoded') {
    $body = json_decode(json_encode($_POST));
}

if (!empty($request)) {
    if (file_exists("routes/$request.php")) {
        require_once "routes/$request.php";
    } else {
        http_response_code(404);
        echo json_encode(array("message" => "Endpoint not found"));
    }    
} else {
    http_response_code(200);
    echo json_encode(array(
        "title" => "Ra Integration API",
        "version" => "v0.0.1",
        "description" => "External API Ra integration service"
    ));
}
// Hello Just Testing