<?php
function verifyToken($token, $platform = null) {
    try {
        list($header_decoded, $payload_decoded, $signature_decoded) = explode('.', $token);
        $decoded_payload = json_decode(base64_decode($payload_decoded), true);
        $expected_signature = hash_hmac('sha256', "$header_decoded.$payload_decoded", JWT_SECRET, true);
        $expected_signature_base64 = base64_encode($expected_signature);
        if ($signature_decoded === $expected_signature_base64) {
            if (isset($platform)) {
                if ($platform != $decoded_payload["platform"]) {
                    throw new \Exception("403;You don't have permission.");
                }
            }
            return true;
        } else {
            throw new \Exception("401;Invalid token, signature verification failed.");
        }
    } catch (Exception $e) {
        $error = explode(";", $e->getMessage());
        http_response_code($error[0]);
        echo json_encode(array("message" => $error[1]));
        exit();
    }
}

function external() {
    $headers = apache_request_headers();
    if (isset($headers['Mid-Api-Key']) || isset($headers['mid-api-key'])) {
        $token = $headers['Mid-Api-Key'] ?? $headers['mid-api-key'];
        $jwt = verifyToken($token, 'external');
    } else {
        http_response_code(401);
        echo json_encode(array("message" => "Api-key is required."));
        exit();
    }
}