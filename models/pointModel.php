<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Point {
  private $db;
  private $conn;
  private $table = 'points';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function add_point ($data) {
    try {

      $sql = "
        INSERT INTO $this->table (
          member_id,
          phone_number,
          point,
          operation,
          note
        ) VALUES (?,?,?,?,?) RETURNING *
      ";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssiss", 
        $data['member_id'],
        $data['phone_number'],
        $data['point'],
        $data['operation'],
        $data['note']
      );
      $stmt->execute();
      $result = $stmt->get_result();
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }
  
      $stmt->close();
      return $row;
    } catch(\Exception $e) {
      throw $e;
    }
  }
}