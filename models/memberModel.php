<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Member {
  private $db;
  private $conn;
  private $table = 'members';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function getByPhone ($phone) {
    $sql = "
      SELECT 
        code as memberCode,
        (SELECT name FROM cards WHERE total_point_balance BETWEEN min_points AND max_points LIMIT 1) as tier,
        title,
        TRIM(CONCAT(first_name, ' ', last_name)) as name,
        email,
        phone_number as phoneNumber,
        birth_date as birthDate,
        gender,
        country,
        city,
        address,
        total_point_balance as totalPointBalance,
        room_preference as roomPreference,
        food_preference as foodPreference,
        internal_preference as internalPreference,
        register_date as registerDate,
        IF(is_active = 1, 'true', 'false') as isActive
      FROM $this->table 
      WHERE phone_number = ? OR code = ?
    ";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("ss", $phone, $phone);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function getByCode($code) {
    $sql = "
      SELECT 
        id,
        code as memberCode,
        (SELECT name FROM cards  WHERE total_point_balance BETWEEN min_points AND max_points LIMIT 1) as tier,
        title,
        TRIM(CONCAT(first_name, ' ', last_name)) as name,
        email,
        phone_number,
        birth_date,
        gender,
        country,
        city,
        address,
        total_point_balance as totalPointBalance,
        room_preference as roomPreference,
        food_preference as foodPreference,
        internal_preference as internalPreference,
        register_date as registerDate,
        IF(is_active = 1, 'true', 'false') as isActive
      FROM $this->table 
      WHERE code = ?
    ";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $code);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function update_point($data) {
    try {
      
      $sql = "UPDATE $this->table SET total_point_balance=? WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("is", $data['point'], $data['id']);
      $stmt->execute();
      $result = $stmt->get_result();

      $stmt->close();
      return true;
    } catch(\Exception $e) {
      throw $e;
    }
  }

  public function add_point($data) {
    try {
      $columns = ['member_id','phone_number','point','operation','note'];
      $values = [];
      foreach ($data as $row) {
        $val = [];
        $val["member_id"] = "";
        $val["phone_number"] = $row["phone_number"];
        $val["point"] = $row["total_point_balance"];
        $val["operation"] = "in";
        $val["note"] = "vhp"; // add transaction from hotel / outlet ex: vhp_hotel

        $valueString = "(" . implode(', ', array_map(function($value) {
          return "'" . addslashes($value) . "'";
        }, $val)) . ")";
        $values[] = $valueString;
      }
      $sql = "INSERT INTO points (" . implode(', ', $columns) . ") VALUES " . implode(', ', $values);
      $stmt = $this->conn->prepare($sql);
      
      $stmt->execute();
      $result = $stmt->get_result();
  
      $stmt->close();
      return true;
    } catch(\Exception $e) {
      throw $e;
    }
  }
    
  public function member_sync($params) {
    try {
      $data = sizeof($params) > 0 ? $params : [];
      $columns = [
        'code',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone_number',
        'tier',
        'title',
        'birth_date',
        'gender',
        'country',
        'city',
        'address',
        'total_point_balance',
        'room_preference',
        'food_preference',
        'internal_preference',
        'register_date',
        'is_active',
        'is_sync'
      ];

      $values = [];
      $update = [];
      $points = [];

      foreach ($data as $row) {
        $row = json_decode(json_encode($row), true);

        $point = [];
        $point['phone_number'] = $row['phone_number'];
        $point['total_point_balance'] = $row['total_point_balance'];

        $points[] = $point;

        $valueString = "(" . implode(', ', array_map(function($value) {
          return "'" . addslashes($value) . "'";
        }, $row)) . ")";
        $values[] = $valueString;
      }
      
      foreach ($columns as $column) {
        $update[] = "$column = VALUES($column)";
      }

      $sql = "INSERT INTO members (" . implode(', ', $columns) . ") VALUES " . implode(', ', $values) .
       " ON DUPLICATE KEY UPDATE " . implode(', ', $update);
      $stmt = $this->conn->prepare($sql);
      
      $stmt->execute();
      $result = $stmt->get_result();
  
      $stmt->close();
  
      $this->add_point($points);
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getVouchers ($params) {
    try { 
      $sql = "with
        amount_plus as (
          select 
            v.article_number as articleNumber,
            v.code as voucherCode,
            v.description,
            v.minimum_spending as minimumSpending,
            v.plu as plu,
            v.title as title,
            v.discount as discount,
            v.amount,
            r.member_code,
            v.expire_date as expiredAt,
            v.type as type
          from 
            vouchers v 
            left join redeem r on r.voucher_id = v.id and r.status = 'redeem'
          where true
            and r.member_code = ?
            and v.outlet_code = ?
            and v.status != 'redeemed'
        ),
        amount as (
          select 
              COALESCE (
                JSON_ARRAYAGG(
                	case when a.amount = 0 then 
                  JSON_OBJECT(
                    'articleNumber', a.articleNumber,
                    'voucherCode', a.voucherCode,
                    'description', a.description,
                    'discount', a.discount,
                    'minimumSpending', a.minimumSpending,
                    'expiredAt', a.expiredAt
                  )
                  else 
                  JSON_OBJECT(
                    'articleNumber', a.articleNumber,
                    'voucherCode', a.voucherCode,
                    'description', a.description,
                    'amount', a.amount,
                    'minimumSpending', a.minimumSpending,
                    'expiredAt', a.expiredAt
                  )
                  end
                ), JSON_ARRAY()
              )
            as amount
          from 
            amount_plus a 
          where 
            a.type = 'AMOUNTS'
        ),
        plus as (
          select 
            COALESCE (
              JSON_ARRAYAGG(
                JSON_OBJECT(
                  'articleNumber', a.articleNumber,
                  'voucherCode', a.voucherCode,
                  'description', a.description,
                  'plu', a.plu,
                  'pluName', a.title,
                  'discount', a.discount,
                  'price', a.amount,
                  'expiredAt', a.expiredAt
                )
              ), JSON_ARRAY()
            ) as plus
          from 
            amount_plus a 
          where 
            a.type = 'PLUS'
        ),
        bills_plus as (
          select 
            b.article_number as articleNumber,
            b.code as codeBenefits,
            b.description as description,
            b.plu as plu,
            b.title as title,
            b.discount as discount,
            b.expire_date as expiredAt,
            b.type as type,
            b.price as price
          from 
            benefits b 
            left join member_benefits mb on mb.benefits_id = b.id
            join members m on m.id = mb.member_id
          where true
            and m.code = ?
            and b.outlet_code = ?
        ),
        plus_benefit as (
          select 
            JSON_ARRAYAGG(
              JSON_OBJECT(
                'articleNumber', bp.articleNumber,
                'benefitId', bp.codeBenefits,
                'description', bp.description,
                'plu', bp.plu,
                'pluName', bp.title,
                'discount', bp.discount,
                'price', bp.price
              )
            ) as plus
          from 
            bills_plus bp
           where 
            bp.type = 'PLUS'
        ),
        test_bills as (
          select 
            JSON_ARRAYAGG(
              JSON_OBJECT(
                'articleNumber', bp.articleNumber,
                'benefitId', bp.codeBenefits,
                'description', bp.description,
                'discount', bp.discount,
                'price', bp.price
              )
            ) as test_bills
          from 
            bills_plus bp
           where 
            bp.type = 'BILLS'
        )
        select 
          JSON_OBJECT(
            'plus', COALESCE (p.plus, JSON_ARRAY()),
            'amounts', COALESCE (am.amount, JSON_ARRAY())
          ) as vouchers,
          JSON_OBJECT(
            'plus', COALESCE (pb.plus, JSON_ARRAY()),
            'bills', COALESCE (tb.test_bills, JSON_ARRAY())
          ) as benefits
        from 
          plus p,
          amount am,
          amount_plus a,
          plus_benefit pb,
          test_bills tb
        LIMIT 1;";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sisi", $params->memberCode, $params->outletCode, $params->memberCode, $params->outletCode);
      
      $stmt->execute();
      $result = $stmt->get_result();
      $rows = [];

      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $rows['vouchers'] = json_decode($row['vouchers']);
        $rows['benefits'] = json_decode($row['benefits']);
      }
  
      $stmt->close();
      return $rows;
    } catch (\Exception $e) { 
      throw $e; 
    }
  }
}