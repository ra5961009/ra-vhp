<?php
namespace Models;

class Voucher {
  private $conn;
  private $table = 'vouchers';

  public function __construct($conn) {
    $this->conn = $conn;
  }

  public function burn($params) {
    try {
      $sql = "INSERT INTO redeem
      (
        member_id,
        voucher_id,
        member_code,
        voucher_code,
        outlet_code,
        license_id,
        cashier_id,
        cashier_name,
        status
      ) VALUES (?,?,?,?,?,?,?,?,'redeemed')";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssssssss", 
        $params->memberId,
        $params->voucherId,
        $params->memberCode,
        $params->voucherCode, 
        $params->outletCode,
        $params->licenseId,
        $params->cashierId,
        $params->cashierName
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function unburn($params) {
    try {
      $sql = "INSERT INTO redeem
      (
        member_id,
        voucher_id,
        member_code,
        voucher_code,
        outlet_code,
        license_id,
        cashier_id,
        cashier_name,
        status
      ) VALUES (?,?,?,?,?,?,?,?,'unredeem')";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssssssss", 
        $params->memberId,
        $params->voucherId,
        $params->memberCode,
        $params->voucherCode, 
        $params->outletCode,
        $params->licenseId,
        $params->cashierId,
        $params->cashierName
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function updateByCode($code, $status) {
    try {
      $sql = "UPDATE $this->table SET status = '$status' WHERE code = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      
      $stmt->execute();
      $result = $stmt->get_result();
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getByCode($code) {
    try {
      $sql = "SELECT * FROM $this->table  WHERE true and deleted_at is null and code = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getActiveCode($code) {
    try {
      $sql = "SELECT * FROM $this->table  WHERE true and deleted_at is null and code = ? and DATE(expire_date) >= DATE(NOW()) and status in ('redeem','unredeem')";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getInactiveCode($code) {
    try {
      $sql = "SELECT * FROM $this->table  WHERE true and deleted_at is null and code = ? and status = 'redeemed'";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getRedeemCode($code) {
    try {
      $sql = "SELECT * FROM redeem  WHERE true and deleted_at is null and voucher_code = ? and status = 'redeemed'";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }
  
  public function getAll ($limit, $offset, $search, $sort) {
    try {
      $src = '';
      $order = '';
      if ($search) $src .= "and (code like '%$search%' or title like '%$search%')";
      if (sizeof($sort) == 2) $order .= "ORDER BY $sort[0] $sort[1]";

      $sql = "
        SELECT 
          *, 
          c.full_count 
        FROM 
          $this->table 
          RIGHT JOIN (SELECT count(*) AS full_count FROM $this->table WHERE true and deleted_at is null $src) c ON true
        WHERE true
          and deleted_at is null
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (Exception $e) {
      throw $e;
    }
  }
  public function getByHotelId ($id) {
    // $sql = "SELECT * FROM $this->table WHERE id = ?";
    $sql = "SELECT * FROM $this->table where deleted_at is null";
    $stmt = $this->conn->prepare($sql);
    // $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $rows = [];
    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
      }
    }

    $stmt->close();
    return $rows;
  }
}