<?php
namespace Models;

class Transaction {
  private $conn;
  private $table = 'transactions';

  public function __construct($conn) {
    $this->conn = $conn;
  }

  public function bulk_insert($params) {
    try {
      $sql = "INSERT INTO $this->table
      (
        transaction_type,
        token,
        hotel_code,
        member_code,
        store,
        invoice_number,
        total_value,
        number_of_people,
        tax,
        service,
        discount,
        check_in_date,
        check_out_date,
        table_number,
        open_table_date,
        closed_table_date,
        split_number
      ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sssssssssssssssss", 
        $params->transaction_type,
        $params->token,
        $params->hotel_code,
        $params->user,
        $params->store,
        $params->invoice_number,
        $params->total_value,
        $params->number_of_people,
        $params->tax,
        $params->service,
        $params->discount,
        $params->check_in_date,
        $params->check_out_date,
        $params->table_number,
        $params->open_table_date,
        $params->closed_table_date,
        $params->split_number
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }
      
      $stmt->close();
      return $row;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function bulk_insert_item($params) {
    try {
      $sql = "INSERT INTO transaction_details
      (
        transaction_id,
        product_name,
        quantity,
        price,
        rate_code,
        room_type
      ) VALUES (?,?,?,?,?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssssss", 
        $params->transaction_id,
        $params->product_name,
        $params->quantity,
        $params->price,
        $params->ratecode,
        $params->roomtype
      );
      
      $stmt->execute();
      $stmt->close();

      
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function bulk_insert_voucher($params) {
    try {
      $sql = "INSERT INTO transaction_vouchers
      (
        transaction_id,
        voucher_code
      ) VALUES (?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ss", 
        $params->transaction_id,
        $params->voucher_code
      );
      
      $stmt->execute();
      $stmt->close();

      
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function bulk_insert_benefit($params) {
    try {
      $sql = "INSERT INTO transaction_benefits
      (
        transaction_id,
        benefit_id
      ) VALUES (?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ss", 
        $params->transaction_id,
        $params->benefit_id
      );
      
      $stmt->execute();
      $stmt->close();

      
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function unburn($params) {
    try {
      $sql = "INSERT INTO redeem
      (
        member_id,
        voucher_id,
        member_code,
        voucher_code,
        outlet_code,
        license_id,
        cashier_id,
        cashier_name,
        status
      ) VALUES (?,?,?,?,?,?,?,?,'unredeem')";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssssssss", 
        $params->memberId,
        $params->voucherId,
        $params->memberCode,
        $params->voucherCode, 
        $params->outletCode,
        $params->licenseId,
        $params->cashierId,
        $params->cashierName
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function updateByCode($code, $status) {
    try {
      $sql = "UPDATE $this->table SET status = '$status' WHERE code = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      
      $stmt->execute();
      $result = $stmt->get_result();
      return true;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getByCode($code) {
    try {
      $sql = "SELECT * FROM $this->table  WHERE true and deleted_at is null and code = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getActiveCode($code) {
    try {
      $sql = "SELECT * FROM $this->table  WHERE true and deleted_at is null and code = ? and DATE(expire_date) >= DATE(NOW()) and status in ('redeem','unredeem')";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getInactiveCode($code) {
    try {
      $sql = "SELECT * FROM $this->table  WHERE true and deleted_at is null and code = ? and status = 'redeemed'";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getRedeemCode($code) {
    try {
      $sql = "SELECT * FROM redeem  WHERE true and deleted_at is null and voucher_code = ? and status = 'redeemed'";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $code);
      $stmt->execute();
      $result = $stmt->get_result();
  
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }
}