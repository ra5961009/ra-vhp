<?php
namespace Controller;
require_once("models/userModel.php");
require_once("models/memberModel.php");

use Models\User;
use Models\Member;

class UserController {
  private function getToken($payload) {
    $header = base64_encode(json_encode(["alg" => "HS256", "typ" => "JWT"]));
    $payload_json = base64_encode(json_encode($payload));
    
    $signature = hash_hmac('sha256', "$header.$payload_json", JWT_SECRET, true);
    $signature_base64 = base64_encode($signature);
    
    $jwt_token = "$header.$payload_json.$signature_base64";
    
    return $jwt_token;
  }

  public function login($body) {
    if (!isset($body->username) || !isset($body->password) || !isset($body->platform)) {
      throw new \Exception("400;Mandatory parameter[s].");
    }
    
    if(!in_array($body->platform, array('web','mobile'))) {
      throw new \Exception("400;Mandatory parameter[s].");
    }

    $result = null;
    $payload = null;
    if ($body->platform != 'web') {
      $member = new Member();
      if (filter_var($body->username, FILTER_VALIDATE_EMAIL)) {
        $result = $member->getByEmail($body->username);
      } else {
        $result = $member->getByPhone($body->username);
      }
      
      if (isset($result)) {
        $payload = [
          "id" => $result['id'],
          "first_name" => $result['first_name'],
          "last_name" => $result['last_name'],
          "email" => $result['email'],
          "platform" => $body->platform,
          "exp" => time() + (60 * 480) // Token expiry time (8 hour from now)
        ];
      }
    } else {
      $user = new User();
      $result = $user->getByEmail($body->username);
      if (isset($result)) {
        $payload = [
          "id" => $result['id'],
          "name" => $result['name'],
          "email" => $result['email'],
          "role" => $result['role'],
          "platform" => $body->platform,
          "exp" => time() + (60 * 480) // Token expiry time (1 hour from now)
        ];
      }
    }

    $password = isset($result) ? $result['password'] : '';
    $password_validate = password_verify($body->password, $password);
    
    if ($password_validate) {
      $token = $this->getToken($payload);
      $result["access_token"] = $token;
      unset($result["password"]);
      
      return $result;
    } else {
      throw new \Exception("400;Check username or password.");
    }
  }
}