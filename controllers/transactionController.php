<?php
namespace Controller;
require_once("models/transactionModel.php");
require_once("models/pointModel.php");
require_once("models/memberModel.php");
include_once("utils/error.php");
require_once("models/config.php");

use Models\Transaction;
use Models\Member;
use Models\Point;
use Models\Database;
use Exception;

class TransactionController {
  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function getPoint($total_value) {
    $point = floor($total_value / 100000);
    return $point;
  }

  public function hotel($body)  {
    $this->conn->begin_transaction();
    try {
      if(isset($body->transaction) && sizeof($body->transaction) > 0) {
        $transaction = new Transaction($this->conn);
        $member = new Member();
        $point = new Point();

        $rows = [];
        foreach($body->transaction as $row) {
          $value = (object) array();
          $value->transaction_type = 'hotel';
          $value->token = $row->token;
          $value->hotel_code = $row->hotel_code;
          $value->user = $row->user;
          $value->store = $row->store;
          $value->invoice_number = $row->invoice_number;
          $value->total_value = $row->total_value;
          $value->number_of_people = $row->number_of_people;
          $value->tax = $row->tax;
          $value->service = $row->service;
          $value->discount = $row->discount;
          $value->check_in_date = $row->check_in_date ?? null;
          $value->check_out_date = $row->check_out_date ?? null;
          $value->table_number = $row->table_number ?? null;
          $value->open_table_date = $row->open_table_date ?? null;
          $value->closed_table_date = $row->closed_table_date ?? null;
          $value->split_number = $row->split_number ?? null;

          $result_trx = $transaction->bulk_insert($value);
          if(sizeof($row->items) > 0) {
            $transaction_id = $result_trx['id'];            
            foreach($row->items as $item) {
              $item->transaction_id = $transaction_id;
              $result_item = $transaction->bulk_insert_item($item);
            }
          }

          $res = (object) array();
          $res->invoiceNumber = $value->invoice_number;
          $res->referenceNumber = "";
          $rows[] = $res;

          $get_point = $this->getPoint($row->total_value);
          $data_member = $member->getByCode($row->user);
          if (!$data_member) {
            throw new Exception("Member not found");
            return;
          }

          $new_point = $get_point + $data_member['totalPointBalance'];
          
          // Update point to member
          $update_point = $member->update_point(array("point" => $new_point, "id" => $data_member['id']));
          if(!$update_point) {
            throw new Exception("Invalid update point");
            return;
          }
          
          // Add point to table point
          $data_point = [];
          $data_point['member_id'] = $data_member['id'];
          $data_point['phone_number'] = $data_member['phone_number'];
          $data_point['point'] = $get_point;
          $data_point['operation'] = "in";
          $data_point['note'] = "vhp_hotel";
          $add_point = $point->add_point($data_point);
          if(!$update_point) {
            throw new Exception("Invalid add point");
            return;
          }

          $this->conn->commit();
        }

        http_response_code(200);
        echo json_encode(array(
          "data" => $rows,
          "message" => "success",
          "statusCode" => "SUCCESS_POST_REQUEST", 
          "status" => true
        ));
        
        return;
      } else {
        throw new Exception("Invalid parameter");
        return;
      }
    } catch (\Exception $e) {
      $this->conn->rollback();

      if($e->getMessage()) {
        http_response_code(400);
        echo json_encode(array(
          "message" => "You've validation error", 
          "statusCode" => "ERROR_VALIDATION", 
          "status" => false
        ));
        return;
      } else {
        throw $e;
      }
    }
  }

  public function validationHotel($data, $store) {
    switch($data){
      case '0001':
        $allow_store = [0,1,2,4,5];
        if(in_array($store, $allow_store)) {
          return $store;
        } else {
          return null;
        }
        break;
      case '0002':
        $allow_store = [0,1,2,3,4];
        if(in_array($store, $allow_store)) {
          return $store;
        } else {
          return null;
        }
        break;
      case '0003':
        $allow_store = [1,2];
        if(in_array($store, $allow_store)) {
          return $store;
        } else {
          return null;
        }
        break;
    }
  }

  public function validationInvoice($arr, $obj) {
    if (sizeof($arr) > 0) {
      foreach($arr as $o) {
        if ($o->invoice_number == $obj->invoice_number && $o->split_number == $obj->split_number) {
          return false;
        }
        return true;
      }
    } else {
      return true;
    }
  }

  public function outlet($body)  {
    $this->conn->begin_transaction();
    try {
      if(isset($body->transaction) && sizeof($body->transaction) > 0) {
        $transaction = new Transaction($this->conn);
        $member = new Member();
        $point = new Point();
        
        $rows = [];
        $validData = [];
        $invalidData = [];

        foreach($body->transaction as $row) {
          if($row->user != '') {
            $storValid = $this->validationHotel($row->license_id, $row->store);
            if($storValid) {
              $validInvoice = $this->validationInvoice($validData, $row);
              if ($validInvoice) {
                $validData[] = $row;
              } else {
                $invalidData[] = $row;
              }
            } else {
              $invalidData[] = $row;
            }
          } else {
            $invalidData[] = $row;
          }
        }

        if(sizeof($validData) > 0) {
          foreach($validData as $row) {
            $value = (object) array();
            $value->transaction_type = 'outlet';
            $value->hotel_code = $row->license_id;
            $value->user = $row->user;
            $value->store = $storValid;
            $value->invoice_number = $row->invoice_number;
            $value->total_value = $row->total_value;
            $value->number_of_people = $row->number_of_people;
            $value->tax = $row->tax;
            $value->service = $row->service;
            $value->discount = $row->discount;
            $value->check_in_date = $row->check_in_date ?? null;
            $value->check_out_date = $row->check_out_date ?? null;
            $value->table_number = $row->table_number ?? null;
            $value->open_table_date = $row->open_table_date ?? null;
            $value->closed_table_date = $row->closed_table_date ?? null;
            $value->split_number = $row->split_number ?? null;
    
            $value->items = $row->items;
            $value->vouchers = $row->vouchers;
            $value->benefits = $row->benefits;
    
            $result_trx = $transaction->bulk_insert($value);
    
            if(sizeof($row->items) > 0) {
              $transaction_id = $result_trx['id'];
              foreach($row->items as $item) {
                $item->transaction_id = $transaction_id;
                $result_item = $transaction->bulk_insert_item($item);
              }
            }
    
            if(sizeof($row->vouchers) > 0) {
              $transaction_id = $result_trx['id'];
              foreach($row->vouchers as $voucher) {
                $voucher->transaction_id = $transaction_id;
                $result_voucher = $transaction->bulk_insert_voucher($voucher);
              }
            }
    
            if(sizeof($row->benefits) > 0) {
              $transaction_id = $result_trx['id'];
              foreach($row->benefits as $benefit) {
                $benefit->transaction_id = $transaction_id;
                $result_benefit = $transaction->bulk_insert_benefit($benefit);
              }
            }
    
            $res = (object) array();
            $res->invoiceNumber = $value->invoice_number;
            $res->referenceNumber = "";
            $rows[] = $res;
            
            $get_point = $this->getPoint($row->total_value);
            $data_member = $member->getByCode($row->user);
    
            if ($data_member) {
              $new_point = $get_point + $data_member['totalPointBalance'];
    
              // Update point to member
              $update_point = $member->update_point(array("point" => $new_point, "id" => $data_member['id']));
              if(!$update_point) throw new Exception("Invalid update point");
              
              // Add point to table point
              $data_point = [];
              $data_point['member_id'] = $data_member['id'];
              $data_point['phone_number'] = $data_member['phone_number'];
              $data_point['point'] = $get_point;
              $data_point['operation'] = "in";
              $data_point['note'] = "vhp_outlet";
              $add_point = $point->add_point($data_point);
    
              if(!$add_point) {
                throw new Exception("Invalid add point"); 
                return;
              }
            } 
    
            $this->conn->commit();
          }
        }

        http_response_code(200);
        echo json_encode(array(
          "data" => $rows,
          "message" => "success",
          "statusCode" => "SUCCESS_POST_REQUEST", 
          "status" => true
        ));
        
        return;
      } else {
        throw new Exception("Invalid parameter");
        return;
      }
    } catch (\Exception $e) {
      $this->conn->rollback();

      if ($e->getMessage()) {
        $code = $e->getMessage() == 'Store not available' ? 200 : 400;
        http_response_code($code);
        echo json_encode(array(
          "message" => $e->getMessage(), 
          "statusCode" => "ERROR_VALIDATION", 
          "status" => false
        ));
        return;
      } else {
        throw $e;
      }
    }
  }
}