<?php
namespace Controller;
require_once("models/memberModel.php");
include_once("utils/error.php");

use Models\Member;

class MemberController {
  public function getByPhone($phone) {
    $member = new Member();
    $result = $member->getByPhone($phone);
    return $result;
  }

  public function synchronize($body)  {
    try {
      if(sizeof($body) > 0) {
        $filed_memeber = [];
        $member_values = [];
        
        foreach($body as $row) {
          if (!isset($row->phone_number) || !isset($row->name)) {
            $filed_memeber[] = $row;
          } else {
            $value = [];
            $hash = password_hash(strval(isset($row->password) ? $row->password : '1234'), PASSWORD_DEFAULT);
            $name_arr = explode(" ", $row->name);
            
            $value['code'] = $row->code ?? "";
            $value['first_name'] = $name_arr[0];
            $value['last_name'] = isset($name_arr[1]) ? isset($name_arr[2]) ? $name_arr[1] . " " . $name_arr[2] : $name_arr[1] : "";
            $value['email'] = $row->email ?? "";
            $value['password'] = $hash;
            $value['phone_number'] = $row->phone_number;
            $value['tier'] = $row->tier ?? "";
            $value['title'] = $row->title ?? "";
            $value['birth_date'] = $row->birth_date ?? "";
            $value['gender'] = $row->gender ?? "";
            $value['country'] = $row->country ?? "";
            $value['city'] = $row->city ?? "";
            $value['address'] = $row->address ?? "";
            $value['total_point_balance'] = $row->total_point_balance ?? "";
            $value['room_preference'] = $row->room_preference ?? "";
            $value['food_preference'] = $row->food_preference ?? "";
            $value['internal_preference'] = $row->internal_preference ?? "";
            $value['register_date'] = $row->register_date ?? "";
            $value['is_active'] = $row->is_active ?? "";
            $value['is_sync'] = true;

            $member_values[] = $value;
          }
        }
        
        $member = new Member();
        
        $result = $member->member_sync($member_values);

        $memberData = array('fcm_token' => 'abc');
        $params = array(
          'type' => 'token',
          'topic' => '',
          'token' => $memberData['fcm_token'],
          'title' => "You Have Earned 50 Points",
          'body' => "Congratulations! You have earned 50 points for your transaction. Keep earning points and unlock more."
        );
  
        $sendNotif = sendNotif($params);

        http_response_code(200);
        echo json_encode(array(
          "data" => array(
            "synchronized" => $member_values, 
            "unsynchronized" => $filed_memeber,
            "need_to_synchronize" => []
          )
        ));
        return;
      } else {
        http_response_code(400);
        echo json_encode(array("message" => "Mandatory parameter[s]."));
        return;
      }
    } catch (\Exception $e) {
      print_r($e);
      throw $e;
    }
  }

  public function getVouchers($query) {
    $member = new Member();
    $result = $member->getVouchers($query);
    return $result;
  }
}