<?php
namespace Controller;

class TokenController {
  private function getToken($payload) {
    $header = base64_encode(json_encode(["alg" => "HS256", "typ" => "JWT"]));
    $payload_json = base64_encode(json_encode($payload));
    
    $signature = hash_hmac('sha256', "$header.$payload_json", JWT_SECRET, true);
    $signature_base64 = base64_encode($signature);
    
    $jwt_token = "$header.$payload_json.$signature_base64";
    
    return $jwt_token;
  }


  public function generateToken()  {
    try {
      $payload = [
        'owner' => 'vhp',
        'platform' => 'external',
        'iat' => time(),
        'nbf' => time()
      ];
      $token = $this->getToken($payload);
      return $token;
    } catch (\Exception $e) {
      throw $e;
    }
  }
}