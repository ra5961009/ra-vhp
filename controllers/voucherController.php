<?php
namespace Controller;
require_once("models/voucherModel.php");
require_once("models/memberModel.php");
include_once("utils/error.php");
require_once("models/config.php");

use Models\Voucher;
use Models\Member;
use Models\Database;

class VoucherController {
  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function getByHotelsId($id) {
    $voucher = new Voucher($this->conn);
    $result = $voucher->getByHotelId($id);
    return $result;
  }

  public function getAll($query) {
    $page = isset($query->page) ? intval($query->page) : 1;
    $per_page = isset($query->per_page) ? intval($query->per_page) : 20;
    $search = isset($query->search) ? $query->search : null;
    $sort = isset($query->sort) ? explode(":",$query->sort) : [];

    $voucher = new Voucher();
    $offset = ($page - 1) * $per_page;
    $result = $voucher->getAll($per_page, $offset, $search, $sort);
    
    $total = $result["total"];
    $vouchers = $result["rows"];
    
    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    return array("vouchers" => $vouchers, "meta" => $meta);
  }

  public function burn($body)  {
    $this->conn->begin_transaction();
    try {
      if(sizeof($body->vouchers) > 0) {
        $voucher = new Voucher($this->conn);
        $member = new Member();
        $rows = [];
        $used = [];
        foreach($body->vouchers as $row) {
          $res_voucher = $voucher->getActiveCode($row->voucherCode);
          if (!$res_voucher) {
            $row->status = false;
            $row->message = "Voucher $row->voucherCode is already used";
            $used[] = $row;
          } else {
            $value = (object) array();
            $res_member = $member->getByCode($body->memberCode);
            $res_voucher_data = $voucher->getByCode($row->voucherCode);
            if($res_member) {
              $value->memberId = $res_member['id'];
              $value->voucherId = $res_voucher_data['id'];
              $value->memberCode = $body->memberCode;
              $value->voucherCode = $row->voucherCode;
              $value->outletCode = $body->outletCode;
              $value->licenseId = $body->licenseId;
              $value->cashierId = $body->cashierId;
              $value->cashierName = $body->cashierName;
              $result = $voucher->burn($value);
              if ($result) {
                $update = $voucher->updateByCode($row->voucherCode, "redeemed");
                if ($update) {
                  $row->articleNumber = "";
                  $row->amount = "";
                  $row->status = true;
                  $row->message = "success";
                  $rows[] = $row;
                  $this->conn->commit();
                }
              }
            } else {
              http_response_code(400);
              echo json_encode(array("message" => "Member code not found."));
              return;
            }
          }
        }

        if(sizeof($used) > 0) {
          http_response_code(400);
          echo json_encode(array(
            "data" => $used,
            "message" => "Voucher code does not exist or expired", 
            "statusCode" => "ERROR_VALIDATION",
            "status" => false
          ));
        } else {
          http_response_code(200);
          echo json_encode(array(
            "data" => $rows,
            "message" => "Vouchers successfully burned.",
            "statusCode" => "SUCCESS_POST_REQUEST",
            "status" => true
          ));
        }
        return;
      } else {
        http_response_code(400);
        echo json_encode(array("message" => "Mandatory parameter[s]."));
        return;
      }
    } catch (\Exception $e) {
      print_r($e);
      $this->conn->rollback();
      throw $e;
    }
  }

  public function unburn($body)  {
    $this->conn->begin_transaction();
    try {
      if(sizeof($body->vouchers) > 0) {
        $voucher = new Voucher($this->conn);
        $member = new Member();
        $rows = [];
        $notfound = [];
        foreach($body->vouchers as $row) {
          $res_voucher = $voucher->getInactiveCode($row->voucherCode);
          if (!$res_voucher) {
            $row->status = false;
            $row->message = "Voucher $row->voucherCode is not found";
            $notfound[] = $row;
          } else {
            $res_redeem = $voucher->getRedeemCode($row->voucherCode);
            if($res_redeem) {
              $value = (object) array();
              $value->memberId = $res_redeem['member_id'];
              $value->voucherId = $res_redeem['voucher_id'];
              $value->memberCode = $res_redeem['member_code'];
              $value->voucherCode = $row->voucherCode;
              $value->outletCode = $body->outletCode;
              $value->licenseId = $body->licenseId;
              $value->cashierId = $body->cashierId;
              $value->cashierName = $body->cashierName;
              $result = $voucher->unburn($value);
              if ($result) {
                $update = $voucher->updateByCode($row->voucherCode, "unredeem");
                if ($update) {
                  $row->articleNumber = "";
                  $row->status = true;
                  $rows[] = $row;
                }
                $this->conn->commit();
              }
            } else {
              $row->status = false;
              $row->message = "Voucher $row->voucherCode is not found";
              $notfound[] = $row;  
            }
          }
        }

        if(sizeof($notfound) > 0) {
          http_response_code(400);
          echo json_encode(array(
            "data" => $notfound,
            "message" => "Vouchers not found", 
            "statusCode" => "ERROR_VALIDATION",
            "status" => false
          ));
        } else {
          http_response_code(200);
          echo json_encode(array(
            "data" => $rows,
            "message" => "Vouchers successfully unburned.",
            "statusCode" => "SUCCESS_POST_REQUEST",
            "status" => true
          ));
        }
        return;
      } else {
        http_response_code(400);
        echo json_encode(array("message" => "Mandatory parameter[s]."));
        return;
      }
    } catch (\Exception $e) {
      $this->conn->rollback();
      print_r($e);
      throw $e;
    }
  }
}